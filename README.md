# Small programs

Small programs I wrote

# Computer Graphics
These programs are written using js and in some cases webgl and three.js libraries. To launch these porgrams put content of this folder to htdocs folder in some program like xamp.

- dviratis_Pavel_Čerenkov.html - Simle animation of bike.
- 1Uzduotis.html - Simple program that uses fractals to create pictures. (choose a number of iterations to do, I recommend to not do more than 7).
- 2Uzduotis.html - Simple program to learn how movement and rotation of figures works (there are perfectly aligned figures inside each other. Turn on transparent option and decrease opacity to see inner figures)
- 3Uzduotis.html - Model of Corona Virus (use Transparency option to look inside)
- 4Uzduotis (whole folder)- Texture put on semi-randomly generated figure (you need to launch it in something like xamp for texture to be loaded)
- Cyclide_animation.html - its part of my bachelor work. It demonstrates one of ways to calculate position of object on another object in this case cube on Cyclide. 
